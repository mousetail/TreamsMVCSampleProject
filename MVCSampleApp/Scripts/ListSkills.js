﻿function cancel(event) {
    event.preventDefault();

    let number = Number.parseInt(event.delegateTarget.id.slice(4));

    let nameField = $("#name-" + number + " input")
    nameField.replaceWith($("<td>").text($(nameField, "input").attr("value")));
    let descField = $("#desc-" + number + " input")
    descField.replaceWith($("<td>").text($(descField, "input").attr("value")));

    $("#sub-" + number).replaceWith($("<a id=\"but-"+number+"\">").text("Edit").on("click", edit));
    $("#can-" + number).detach();

}

function edit(event) {
    console.log("hello there" + event.delegateTarget.id);
    let number = Number.parseInt(event.delegateTarget.id.slice(4));
    event.preventDefault();

    //These two are for the generic skills menu
    let nameField = $("#name-" + number);
    nameField.html($("<input required style=\"width: 100%; max-width: unset;\" form=\"form-"+number+"\" name=\"Name\">").attr("value", nameField.text().trim()));

    let descField = $("#desc-" + number);
    descField.html($("<input style=\"width: 100%; max-width: unset\" form=\"form-" + number + "\" name=\"Description\">").attr("value", descField.text().trim()));

    let button = $("#but-" + number);
    button.after($("<a>").attr("id", "can-" + number).on("click", cancel).text("cancel"));
    button.after($("<br>"));
    button.after($("<input type=\"submit\" id=\"sub-" + number + "\" form=\"form-"+number+"\">").attr("value", "submit"));
    button.remove();
}

$(".edit-button").on("click", edit);