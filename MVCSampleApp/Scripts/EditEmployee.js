﻿$(".skill-id-select").change(
    (event) => {
        let caller = event.delegateTarget;
        let parent = caller.parentElement.parentElement;

        if (parent.tagName.toLocaleLowerCase() !== "tr") {
            throw new Error("invalid tag name: " + parent.tagName);
        }

        $(parent).find(".skill-row select").attr("name", "skill-id-" + caller.value);
        $(parent).find(".exp-row input").attr("name", "skill-id-" + caller.value);
    }
)