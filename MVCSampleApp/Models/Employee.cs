﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace MVCSampleApp.Models
{
    public class Employee
    {
        public Employee()
        {
            EmployeeSkills = new List<EmployeeSkill>();
        }

        public int ID { get; set; }

        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; } = "";
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; } = "";
        [Display(Name = "Phone Number")]
        //probably not the perfect phone number regular expression, but it will reject some mistakes like letters in the middle
        //The at least 3 digits property is arbitrary
        [RegularExpression(@"^([\d\+\#\s\*]{3,})?$")]
        public string PhoneNumber { get; set; } = ""; //stored as a string so it can contain special characters like '+' or '#'

        public virtual IList<EmployeeSkill> EmployeeSkills { get; set; }
    }

    public class Skill
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; } = "";

        [Required]
        public string Description { get; set; } = "";
    }

    public class EmployeeSkill
    {
        [Key]
        [ScaffoldColumn(false)]
        public int ID { get; set; }
        [Required]
        [Display(Name ="Years of Experience")]
        public int YearsOfExperience { get; set; }
        [Required]
        [ForeignKey("Employee")]
        [ScaffoldColumn(false)]
        public int EmployeeID { get; set; }
        [Required]
        [ForeignKey("Skill")]
        
        public int SkillID { get; set; }
        

        public virtual Skill Skill { get; set; }
        public virtual Employee Employee { get; set; }
    }

    public class EmployeeDBContext: DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<EmployeeSkill> EmployeeSkills { get; set; }
    }
}