﻿using MVCSampleApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSampleApp.Controllers
{
    public class EditEmployeeModelClass
    {
        [Key]
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public Employee Employee { get; set; }
        //public ICollection<Skill> Skills { get; set; }
        //public List<Skill> allSkills;
    }

    public class ListSkillsModelClass
    {
        public IEnumerable<EmployeeSkill> employeeSkills;
        public IEnumerable<Skill> allSkills;
    }

    public class ViewEmployeesController : Controller
    {
        private EmployeeDBContext db = new EmployeeDBContext();

        // GET: ViewEmployees
        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }

        // GET: ListSkills
        [HttpGet]
        public ActionResult ListSkills()
        {
            return View(db.Skills.ToList());
        }

        [HttpGet]
        public ActionResult ListSkillsEmployee(int employeeID)
        {
            return View(new ListSkillsModelClass()
            {
                employeeSkills = db.EmployeeSkills.ToList().FindAll(m => m.EmployeeID == employeeID),
                allSkills = db.Skills.OrderBy(i=>i.Name).ToList()
            });
        }

        [HttpPost]
        public ActionResult ListSkills([Bind(Include ="ID, Name, Description")] Skill s)
        {
            if (ModelState.IsValid)
            {
                if (s.ID == 0)
                {
                    db.Skills.Add(s);
                    db.Entry(s).State = EntityState.Added;
                }
                else
                {
                    db.Entry(s).State = EntityState.Modified;
                }

                db.SaveChanges();
            }

            return ListSkills();
        }

        [HttpPost]
        public ActionResult EditEmployee(Employee e)
        {
            //Console.WriteLine("\n\ne.id = "+e.ID+"\n");
            /*if (db.Entry(e).State != EntityState.Detached)
            {
                return Redirect("smbc-comics.com");
            }*/
            
            if (ModelState.IsValid)
            {
                if (e.ID == 0)
                {
                    db.Employees.Add(e);
                    this.db.Entry(e).State = EntityState.Added;
                }
                else
                {
                    this.db.Entry(e).State = EntityState.Modified;
                }

                db.SaveChanges();

                HashSet<int> sets = new HashSet<int>();
                EmployeeSkill[] skills = e.EmployeeSkills.ToArray();
                foreach (EmployeeSkill k in skills)
                {
                    if (e.ID != 0)
                    {
                        k.EmployeeID = e.ID;
                    }
                    else
                    {
                        throw new Exception("e.ID should not be 0");
                    }
                    

                    if (k.SkillID!=0 && !sets.Contains(k.SkillID))
                    {
                        if (k.ID != 0)
                        {
                            db.Entry(k).State = EntityState.Modified;
                        }
                        else
                        {
                            db.Entry(k).State = EntityState.Added;
                        }
                        sets.Add(k.SkillID);
                    }
                    else if (k.ID != 0)
                    {
                        //Deleted if a) No skill ID is assigned OR b) The same skill ID allready happened
                        //AND c) the skill allready exists
                        db.Entry(k).State = EntityState.Deleted;
                    }
                    
                }

                this.db.SaveChanges();

                return this.RedirectToAction("Index");
            }
            else
            {
                //e.FirstName = "FirstName: " + e.FirstName + " LastName: " + e.LastName + " id: " + e.ID;
                return new HttpNotFoundResult();
                //return EditEmployee(id);
            }
        }

        [HttpGet]
        public ActionResult EditEmployee(int? id)
        {
            Employee e;
            if (id == null || id.Value == 0) //actual ID's start at 1, so 0 must be a unset ID
            {
                e = new Employee();
            }
            else //load an existing employee
            {
                e = db.Employees.Find(id.Value);
                if (e == null)
                {
                    return HttpNotFound();
                }
            }

            e.EmployeeSkills.Add(new EmployeeSkill()
            {
                EmployeeID = e.ID,
            });

            List<Skill> skills = db.Skills.ToList();
            skills.Sort((i, j) => (StringComparer.CurrentCulture.Compare(i.Name, j.Name)));
            skills.Insert(0, new Skill()
            {
                ID = 0,
                Name = "",
            });
            ViewBag.skills = skills;

            return View(e); // new EditEmployeeModelClass() {
                //Employee = e,
                //Skills = skills,
                //allSkills = db.Skills.ToList()
            //}

              //  );
        }
    }
}